#lang scribble/manual
@(require (for-label racket
                     "main.rkt"))

@(define (author-email) "deren.dohoda@gmail.com")

@title[#:tag "top"]{Filesystem for-each}
@author{@(author+email "Deren Dohoda" (author-email))}

@defmodule[filesystem-for-each]
@section{Main Directory Procedures}
@defproc[(filesystem-for-each [dir-proc ffe-procedure?]
                              [file-proc ffe-procedure?]
                              [pth path-string?])
         void?]{A depth-first for-each on the given @racket[pth]. Starting at the given @racket[pth], execute
 @racket[dir-proc], then recurse on any directories, and and then execute @racket[file-proc] on all files. Each
 procedure provided must accept one mandatory @racket[path?] argument and a @racket[#:depth] argument which is
 an integer representing how deep the recursion has gone, starting with @code{0}.}
@defproc[(path->directories+files [pth path-string?])
         [values (listof path?)
                 (listof path?)]]{Produces two @racket[list] values: the first is the list of all
 directories contained in the given @racket[pth]; the second is the list of all files contained
 in the given @racket[pth].}
@defproc[(directory-show [pth path-string?]) void?]{A simple use of @racket[filesystem-for-each] to display
 all files and directories in the given @racket[pth].}
@section{Utilities}
@defproc[(ffe-procedure? [proc procedure?]) boolean?]{Checks whether the given procedure is suitable for use
                                                      in @racket[fileesystem-for-each].}
@defproc[(force-path [maybe-pth (or/c path? string?)]) path?]{Convert a string to a path; if already a path,
 do nothing.}
@defproc[(directory? [pth path-string?]) boolean?]{Determines whether @racket[pth] is a directory.}
@defproc[(file? [pth path-string?]) boolean?]{Determines whether @racket[pth] is a file.}
